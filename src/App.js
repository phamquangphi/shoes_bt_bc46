import logo from "./logo.svg";
import "./App.css";
import ShopShoes from "./ShopShoes/ShopShoes";
import ParentShoes from "./ShopShoes/ParentShoes";

function App() {
  return (
    <div>
      <ShopShoes />
    </div>
  );
}

export default App;
