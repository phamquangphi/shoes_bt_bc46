import React from "react";

const ListShoes = ({ shoes, handlPrDeTail }) => {
  return (
    <div className="col-4 mt-3">
      <div className="card">
        <img src={shoes.image} alt="..." />
        <div className="card-body">
          <p style={{ fontWeight: 700, fontSize: 25 }} className="display-5">
            {shoes.name}
          </p>
          <p style={{ fontSize: 20 }} className="text-danger display-6">
            {shoes.price}$
          </p>
          <button
            className="btn btn-outline-success"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={() => {
              handlPrDeTail(shoes);
            }}
          >
            Xem chi tiet
          </button>
        </div>
      </div>
    </div>
  );
};

export default ListShoes;
