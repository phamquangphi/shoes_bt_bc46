import React, { useState } from "react";
import data from "./data.json";
import ParentShoes from "./ParentShoes";
const ShopShoes = () => {
  const [prDeTail, setPrDeTail] = useState(data[0]);
  const handlPrDeTail = (product) => {
    setPrDeTail(product);
  };
  return (
    <div>
      <div className="mt-3 container">
        <h1 className="text-center"> Shoes Shop</h1>
        <ParentShoes data={data} handlPrDeTail={handlPrDeTail} />
        {/**MODAL*/}

        <div>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex={-1}
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">
                    Modal title
                  </h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="row container">
                    <div className="col-4">
                      <img className="img-fluid" src={prDeTail.image} alt="" />
                    </div>
                    <div className="col-8">
                      <p
                        style={{ fontWeight: 700 }}
                        className="font-wieght-blod display-5"
                      >
                        {prDeTail.name}
                      </p>
                      <p style={{ fontWeight: 300 }} className="mt-3">
                        {prDeTail.description}
                      </p>
                      <p
                        className="font-wieght-blod mt-3 text-danger"
                        style={{ fontSize: 25 }}
                      >
                        {prDeTail.price}$
                      </p>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="button" className="btn btn-primary">
                    Save changes
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopShoes;
