import React from "react";
import ListShoes from "./ListShoes";

const ParentShoes = (props) => {
  const { data, handlPrDeTail } = props;

  return (
    <div>
      <div className="row">
        {data.map((item) => {
          return (
            <ListShoes
              handlPrDeTail={handlPrDeTail}
              shoes={item}
              key={item.id}
            />
          );
        })}
      </div>
    </div>
  );
};

export default ParentShoes;
